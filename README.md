
# Kreator delovnih nalogov - Hermi 

# Opis projekta
Projekt oz. aplikacijo delamo za firmo Hermi, ki jim omogoča beleženje delovnih in dnevnih nalog.

# Naloga
Naša naloga je izdelati popolnoma funkcionalno aplikacijo za izpis delovnih nalog, dnevno spremljanje (vnos datumov montaže s časom odhoda, časom pričetka in konca dela ter časom prihoda), poteka montaže ter na koncu pregled porabljenih ur po posameznem projektu (delovnem nalogu), po posameznem monterju, ter tudi v določenem časovnem obdobju ter tudi konec/zaključek projekta (delovnega naloga).

# Cilji
Ideja je razviti namizno aplikacijo, ki bi delovala na strežniku, ter bi omogočala izpis delovnega naloga, dnevno spremljanje (vnos datumov montaže s časom odhoda, časom pričetka in konca dela ter časom prihoda) poteka montaže ter na koncu pregled porabljenih ur po posameznem projektu (delovnem nalogu), po posameznem monterju, ter tudi v določenem časovnem obdobju ter tudi konec/zaključek projekta (delovnega naloga).

# Tehnologije
- Vue.js

# Razvojno okolje
- WebStorm IDE 

# Sledenje poteku dela
- GitLab

# Način dela
Skupaj kot tim, delo smo si razdelili in napredek beležili na GitLab-u. Če se slučajno zgodila neka napaka ali težava tekom projekta, skupaj smo jih odpravljali in reševali.

## Pogoji za namestitev
Instaliran Node.js -v14.18.0 (link za instalacijo: https://nodejs.org/download/release/v14.18.0/)

## Namestitev projekta (frontend)
```
npm install
```

### Lokalni zagon aplikacije
```
npm run serve
```

### Zagon aplikacije 
Naš frontend in backend so live na serveru in lahko dostopajo samo uporabniki, ki smo jih mi registrirali. 
https://hermirv3.web.app


### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
