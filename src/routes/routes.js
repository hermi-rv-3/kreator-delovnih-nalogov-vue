import DashboardLayout from "../components/DashboardLayout.vue";
import ListaMonterja from "../components/Monterji/WorkersList";
import WorkOrderList from "../components/WorkOrders/WorkOrderList";
import LoginPage from "../components/Login/LoginPage";
import DailyOrdersList from "../components/DailyOrders/DailyOrdersList";
import VehiclesList from "../components/Vehicles/VehiclesList";
import { getAuth, onAuthStateChanged  } from 'firebase/auth'

async function validate (to,form, next) {
  onAuthStateChanged(getAuth(), (user) => {
    if (user) {
      next()
    } else {
      next('/signin')
    }
  })
}

const routes = [
  {
    path: "/signin",
    component: LoginPage
  },
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/DelovneNaloge",
    beforeEnter: validate,
    children: [
      {
        path: "DelovneNaloge",
        name: "DelovneNaloge",
        component: WorkOrderList,
      },
      {
        path: "Monterji",
        name: "Monterji",
        component: ListaMonterja,
      },
      {
        path: "Vozila",
        name: "Vozila",
        component: VehiclesList,
      },
      {
        path: "DnevneNaloge",
        name: "Dnevne naloge",
        component: DailyOrdersList,
      },
    ],
  },
];

export default routes;
