import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import routes from "./routes/routes";
import VueRouter from "vue-router";
import VueApexCharts from 'vue-apexcharts'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Notifications from 'vue-notification'
import store from './store/index.js'
import { initializeApp } from "firebase/app"
import firebaseConfig from './config/authentication/FirebaseConfig.json'

const router = new VueRouter({
  history,
  routes,
  linkExactActiveClass: "active",
});
Vue.config.productionTip = false;
initializeApp(firebaseConfig)
Vue.use(VueAxios, axios)
Vue.use(VueRouter);
Vue.config.productionTip = false
Vue.component('apexchart', VueApexCharts)
Vue.use(VueApexCharts)
Vue.use(Notifications)
new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
