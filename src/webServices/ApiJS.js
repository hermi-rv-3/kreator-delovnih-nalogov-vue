import Settings from '../config/webservices/settings.json'
import Key from '../config/webservices/key.json'
import axios from 'axios'
import {getAuth} from "firebase/auth";

class Response {
    data = []
    statusCode = 0
    message = ''
    totalItems = 0
    page = 0

    constructor(data, statusCode, message, totalItems, page) {
        this.data = data
        this.statusCode = statusCode
        this.message = message
        this.totalItems = totalItems
        this.page = page
    }
}

export default class Api {
    static instance
    baseUrl =  Settings.url
    key = Key.key

    constructor () {}

    static getInstance () {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new Api()
        }
        return this.instance
    }

    checkPath (path) {
        if (path !== '') {
            path = '/' + path
        }
        return path
    }

    async get (url, params, path = '') {
        path = this.checkPath(path)
        let responseData
        if (params == undefined) {
            params = {}
        }
        params['key'] = getAuth().currentUser['accessToken']
        await axios.get(this['baseUrl'] + url + path,{ params: params }).then((response) => {
            if (url === 'dnevne-nalog/search/export') {
                window.open(response["request"]['responseURL'])
            }
            if (response['data']['status'] == 0) {
                throw new Error(response['data']['reason'])
            }
            if (response['data']['meta'] != undefined) {
                responseData = new Response(response['data']['data'],
                    response['status'],
                    response['statusText'],
                    response['data']['meta']['total'],
                    response['data']['meta']['current_page'])
            }else {
                responseData = new Response(response['data']['data'], response['status'], response['statusText'])
            }

        })
        return responseData
    }

    async post (url, params, path = '') {
        path = this.checkPath(path)
        if (params == undefined) {
            params = {}
        }
        params['key'] = getAuth().currentUser['accessToken']
        let responseData
        await axios.post(this['baseUrl'] + url + path, params).then((response) => {
            if (response['data']['status'] == 0) {
                throw new Error(response['data']['reason'])
            }
            if (response['data']['meta'] != undefined) {
                responseData = new Response(response['data']['data'],
                    response['status'],
                    response['statusText'],
                    response['data']['meta']['total'],
                    response['data']['meta']['current_page'])
            } else {
                responseData = new Response(response['data']['data'], response['status'], response['statusText'])
            }
        })
        return responseData
    }

    async put (url, path, params) {
        path = this.checkPath(path)
        params['key'] = getAuth().currentUser['accessToken']
        let responseData
        await axios.put(this['baseUrl'] + url + path, params).then((response) => {
            if (response['data']['status'] == 0) {
                throw new Error(response['data']['reason'])
            }
            responseData = new Response(response['data']['data'], response['status'], response['statusText'])
        })
        return responseData
    }

}

