export default {
    filters: {
        numberFilter (number) {
            if (number === null || number === undefined){
                return ''
            }
            return Number(parseFloat(number).toFixed(2)).toLocaleString('si', {
                minimumFractionDigits: 2
            });
        }
    }
}
