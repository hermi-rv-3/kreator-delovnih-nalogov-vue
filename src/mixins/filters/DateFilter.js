export default {
    filters: {
        datumFilter (podatek) {
            let datum = new Date(podatek)
            if (podatek === null) {
                return null
            }
            return datum.toLocaleDateString('sl-SL')
        }
    }
}
